
First config with ESPEASY

Device: Sonoff Basic

```
           +---------------------------------------+
           |       .....                           |
 blue      +-+     .   .                           |
 +---------+N|     . SONOFF BASIC                +-+   blue
+----------+L|     .   .                         |N+-----------+
 brown     +-+     .....       +---+   +---+     |L+----------+
 (black)   |                   |BTN|   |LED|     +-+   brown
           |                   +---+   +---+       |
           +---------------------------------------+



      GPIO00 | BUTTON
      GPIO12 | RELAY
      GPIO13 | LED1
      GPIO14 | on pinheader
```

Rest of Infrastructure:
- Central OpenWRT Router TP-Link TL-WR1043N
- Raspberry Pi with https://github.com/gcgarner/IOTstack

Overview:

![alt text](swisslamp1.png "config Main")

In Config:
- *Name* is set
- *SSID* and *WPA Key* for Wifi is set
- *Unit nr* is unique for each esp switch
- *Protocol* is *OpenHAB MQTT*
- *Controller* IP from MQTT server
- in *Optional Settings*: Hard wired/static IP Address

The IP of the Sonoff Basic was also set to static in OpenWRT Router

![alt text](swisslamp2.png "config Config")

Hardware Settings: most probably all default

![alt text](swisslamp3.png "config Hardwre")

Devices: Only one, the relay

![alt text](swisslamp4.png "config Devices")

Device 'Relay' datails/edit view:

![alt text](swisslamp5.png "config Relay")

Rules Settings:

- Turn off: http://192.168.1.23/control?cmd=event,TurnOff
- Turn on: http://192.168.1.23/control?cmd=event,TurnOn
- Read all Status:  mosquitto_sub -h 192.168.1.42 -v -t "#"

![alt text](swisslamp6.png "config Rules")

Tools Settings:

After activation of 'Advanced mode' the checkbox on "Rules" was activated:

![alt text](swisslamp7.png "config Tools")


