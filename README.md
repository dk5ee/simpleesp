# simpleESP

simple esp, collecting information to make ESP8266 simple

with lots of todo-points

TODOs: 
* write setup for plugdev/uart/udev rules access for linux users, check with systemd
* write setup for flashing esp with raspberry uart
* compare capabilities and limitations of the alternative firmwares
* collect pinouts and information about common ESP8266 devices

/blankimage/ contains empty flash images. This is needed when a new firmware does not work. Sometimes new firmwares are smaller than the flash size, and by burning the new firmware some artifacts can remain, like for example old configuration settings.

my recommended setup:
* usb to ttl converter, FTDI or CP210x
* udev rules and group for uart for the user, or burn as root
* debian or ubuntu operation system
* apt install esptool

for esptool see: https://github.com/themadinventor/esptool

using esptool:
* find tty device in /dev/ttyUSBx or /dev/ttyACMx
* get flash size: esptool --port /dev/ttyUSB3 flash_id

programm the esp8266 without over-the-air update
* connect esp to usb converter, 
* press reset button, or if not existant, connect GPIO0 to ground
* attach usb to computer, or connect +3.3V at last.

flashing blank image (adjust fs setting, tty device and name of firmware file)

esptool --port /dev/ttyUSB3 write_flash -fs 1MB -fm dout 0x0 blank_1MB.bin 

=====

small list of firmware alternatives:
* espeasy https://www.letscontrolit.com/wiki/index.php/ESPEasy
* tasmota https://github.com/arendst/Tasmota
* esphome https://esphome.io/
* espurna https://github.com/xoseperez/espurna
* nodemcu http://nodemcu-build.com/index.php
* on plattformio: https://docs.platformio.org/en/latest/platforms/espressif8266.html
* with arduino: https://github.com/esp8266/Arduino